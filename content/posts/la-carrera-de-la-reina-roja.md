---
title: La carrera de la reina roja. Cómo cambiar y seguir siendo fiel a ti mismo
date: 2020-08-23
drafts: false
---
>Cambiarse a uno mismo es enormemente complicado. Una parte de nosotros quiere el cambio, lucha para mejorar, y otra se resiste. Parece que, por mucho que nos esforcemos, nunca es suficiente. Por si fuera poco, cuando no conseguimos librarnos de nuestros vicios y malos hábitos, nos sentimos frustrados y nuestra autoestima se resiente.
>
>¿Es posible cambiar sin sufrimiento?

Este, querido simio, es un problema de perspectiva.

La razón por la que debes cambiar no es que seas insuficiente o inadecuado. Es que el mundo a tu alrededor cambia.

Los biólogos que estudian los mecanismos de la evolución llaman a este fenómeno la carrera de la reina roja. El origen de la expresión está en el libro de Lewis Carroll "Alicia a través del espejo"; en él, la Reina Roja le explica a Alicia:

*Este es el tipo de lugar en el que debes correr constantemente para poder mantenerte en el mismo sitio*.

Esta paradoja es útil para entender la carrera evolutiva. Cuando los árboles crecen más y más altos, los animales herbívoros deben desarrollar cuellos y miembros más y más largos. Cuando las presas corren más, los predadores deben adoptar nuevas estrategias, como un pelaje que les ayude a confundirse con el entorno.

Curiosamente, no cambiamos para ser mejores. Cambiamos para seguir siendo iguales, para mantener nuestro lugar. Esta es la primera paradoja del cambio.

El cambio existe, es una realidad inevitable. Como dicen los budistas, todo cambia, nada permanece. Las relaciones, las personas a tu alrededor, las circunstancias políticas y económicas, incluso tus necesidades. En ese contexto, cambiarte a ti mismo no es convertirte en una persona diferente. Es adaptar tus estrategias, tus hábitos, tus creencias, tus decisiones, tus acciones y tus principios para responder a la realidad.

Cambiar es ser fiel a tu naturaleza. Es conectar con tu esencia. Es el esfuerzo de seguir siendo tú mismo en un mundo en movimiento constante.

La segunda paradoja es que, para poder cambiar, debes seguir queriendo ser quien eres.

Los cambios que haces pensando en la mirada de los demás, pensando en que no eres suficiente, pensando en que no eres adecuado, terminan siendo ellos mismos cosméticos, insuficientes e inadecuados.

Aceptarte es el primer paso para el cambio. Tratar de *ser* mejor te hace sentir peor contigo mismo. Cambia, entonces, para *estar* mejor. Cambia para resolver un problema, para ajustarte a una nueva situación, para aprender algo nuevo. Cambia tus estrategias, tus herramientas, tus creencias, pero tú, tú no cambies. Cámbialo todo para seguir siendo tú.

Eres un mono. El miedo, la inseguridad, la frustración y la vulnerabilidad son parte de ti, de tu naturaleza. Vive con ellos, porque tratar de eliminarlos es como cortar tus brazos o tus piernas. El simio perfecto solo existe en los cuentos. No hay nada malo en ti.

¿Estás insatisfecho contigo mismo? Aprende a aceptarte, y a quererte. Corre para seguir en tu sitio. Cambia para no cambiar.
