---
title: El simio en el espejo
description: Filosofía práctica y cuentos para pensar
---
Cada día, mientras me lavo la cara, un simio me devuelve la mirada desde el otro lado del espejo. Es una criatura curiosa que siempre me está haciendo preguntas y, si no le respondo, le dan crisis existenciales. Estas son las historias que le cuento y las cosas que le pienso para que sea un poquito más feliz.
